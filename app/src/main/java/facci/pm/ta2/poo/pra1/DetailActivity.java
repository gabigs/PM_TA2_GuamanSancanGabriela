package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6

        // Aqui en esta linea de codigo se esta recibiendo como parametro el object_id//
        String object_id = getIntent().getStringExtra("object_id");
        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            //En esta linea se accede a las propiedades del metodo done
            @Override
            public void done(DataObject object, DataException e) {
                if(e == null){
                    TextView title = (TextView)findViewById(R.id.nombre);

                    TextView precio = (TextView)findViewById(R.id.precio);
                    ImageView thumbnail = (ImageView)findViewById(R.id.thumbnail);
                    TextView descripcion = (TextView)findViewById(R.id.descripcion);

                    //
                    title.setText((String) object.get("nombre"));
                    precio.setText((String) object.get("precio")+"\u0024");
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));
                    descripcion.setText((String) object.get("descripcion"));

                }
                else {

                }

            }
        });


        // FIN - CODE6

    }

}






















